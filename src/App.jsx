import React from "react";
import logo from './logo.svg';
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
    state = {
        openFirstModal: "false",
        openSecondModal: "false"
    }
    render() {
        const handleClickFirst = () => {
            this.setState({
                ...this.state,
                openFirstModal: this.state.openFirstModal === "false" ? "true" : "false"
            })
        }
        const handleClickSecond = () => {
            this.setState({
                ...this.state,
                openSecondModal: this.state.openSecondModal === "false" ? "true" : "false"
            })
        }
        const texts = {
            headerFirst: "Do you want to delete this file?",
            headerSecond: "Confirm delete",
            bodyFirst: "Once you delete this file, it won't be possible to undo this action.",
            bodySecond: "If you do this, it will be permanent!"
        }
        return (
            <>
            <div>
                <Button handleClick={handleClickFirst} text="Open first modal" backgroundColor="teal"/>
                <Button handleClick={handleClickSecond} text="Open second modal" backgroundColor="blue"/>
            </div>
                {this.state.openFirstModal === "true" ? <Modal handleClick={handleClickFirst} text1={texts.headerFirst} text2={texts.bodyFirst} theme="danger" isClosed={true}/> : null}
                {this.state.openSecondModal === "true" ? <Modal handleClick={handleClickSecond} text1={texts.headerSecond} text2={texts.bodySecond} theme="primary" isClosed={false}/> : null}
            </>
        )
    }
}

export default App;
