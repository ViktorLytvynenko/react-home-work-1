import style from "./Button.module.scss";
const Button = (props) => {
    return (
            <button type="button" onClick={props.handleClick} className={style.btn} style={{background: props.backgroundColor}}>{props.text}</button>
    )
}

export default Button